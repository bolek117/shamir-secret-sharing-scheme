﻿using System;
using System.Numerics;
using NUnit.Framework;
using ShamirsSecretSharingScheme.Services;

namespace ShamirsSecretSharingSchemeTests.Services
{
    [TestFixture()]
    public class MersennePrimesGeneratorTests
    {
        [Test()]
        public void GetTest()
        {
            Assert.AreEqual(new BigInteger(3), MersennePrimesGenerator.Get(1));
            Assert.AreEqual(new BigInteger(524287), MersennePrimesGenerator.Get(7));

            Assert.DoesNotThrow(() => MersennePrimesGenerator.Get(20));
            Assert.Throws<IndexOutOfRangeException>(() => MersennePrimesGenerator.Get(-1));
            Assert.Throws<IndexOutOfRangeException>(() => MersennePrimesGenerator.Get(int.MaxValue - 100));
        }
    }
}