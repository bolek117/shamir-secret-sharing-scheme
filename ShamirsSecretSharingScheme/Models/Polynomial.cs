﻿using System.Numerics;
using System.Linq;
using ShamirsSecretSharingScheme.Services;

namespace ShamirsSecretSharingScheme.Models
{
    public class Polynomial
    {
        public BigInteger[] Coefficients { get; }
        private readonly BigInteger _prime;

        private Polynomial(BigInteger[] coefficients)
        {
            Coefficients = coefficients;
            _prime = Common.BasePrime;
        }

        public BigInteger EvaluateAt(int x)
        {
            var accum = new BigInteger(0);

            foreach (var coefficient in Coefficients.Reverse())
            {
                accum *= x;
                accum += coefficient;
                accum %= _prime;
            }

            return accum;
        }

        public static Polynomial Create(uint degree, BigInteger prime)
        {
            var coefficients = BigIntegerRandomGenerator.GetMultiple(prime, degree);
            return new Polynomial(coefficients);
        }
    }
}