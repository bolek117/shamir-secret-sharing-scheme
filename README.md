# Description
C# implementation of Shamir's Secret Sharing Scheme.
Based on Wikipedia's Python code licensed under CC0 1.0 license - [https://en.wikipedia.org/wiki/Shamir%27s_Secret_Sharing#Python_example]

# Licensing
Licensed under CC0 1.0 Universal license [https://creativecommons.org/publicdomain/zero/1.0]